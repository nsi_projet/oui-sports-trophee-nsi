Pour démarer l'application, ouvrez une application pouvant lire les programes python (voir dans fichier pre-requis).
Ouvrez le fichier de code et exécutez-le.
Dans la fenêtre qui s'affiche, cliquez sue le bouton 'start'. 
Deux jeux sont disponibles : un jeu de course et un jeu de ping-pong.
Choissez le jeu et cliquez sur l'image corespondante.
Une fenêtre contenant les instructions de jeu s'affiche : pour les deux jeux, il faut utiliser les touches haut et bas de votre clavier. 
Cliquez sur n'importe quelle touche du clavier pour commencer le jeu. Pour revenir à la sélection de jeu, cliquez sur la flèche en haut à gauche.

Dans le jeu de course, il faut pendant une minute éviter les obstaces en courant. Pour cela il faut changer de couloir de course avec les touches haut et bas du clavier.
Pour le jeu ping-pong, il faudra gagner sur son adversaire. Le premier qui aura 20 points gagnera.
Pour déplacer la raquette e ping-pong, il faut utiliser les touches haut et bas du clavier.
Les scores respectifs des joueurs qui évolus sont marqué à droite de l'écran.
Une fois la partie terminé, si vous avez gagner une petit coupe s'affichera.
Il y aura une flèche sur laquelle vous pourrez cliquer pour revenir à l'écran de sélection des jeux.
De même si vous perder.
Un dessin s'affichera et une flèche en haut à gauche sera clicable pour revenir à la page de sélection.
