#importer les librairies
import pygame, sys
import time
from random import randint
import random

# pygame setup
pygame.init()
pygame.display.set_caption('Oui Sport')
screen = pygame.display.set_mode((500, 300))
clock = pygame.time.Clock()
running=True

# mesurer le temps
class Minuteur(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.seconds=0
        self.TpsZero=0
    def temps(self):
        self.seconds = (pygame.time.get_ticks() - self.TpsZero) / 1000

temps=Minuteur()
devent = pygame.font.SysFont('Console',25)
player_score, opponent_score = 0, 0

"""
classe qui serra utilisé pour toutes les pages d'acceuil
On y importe les images que nous utiliserons.
Celle avec lesquels on va interagir on utilise la fonction get_rect
"""
class Aceuill(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.background=pygame.image.load('RunPlace.jpg')
        self.image=pygame.image.load('element.png')
        self.bouton=pygame.image.load('StartButton.png')
        self.recstarte=self.bouton.get_rect()
        self.recstarte.x=175
        self.recstarte.y=150
        self.logo01=pygame.image.load('image course.png')
        self.rec=self.logo01.get_rect()
        self.rec.x=50
        self.rec.y=50
        self.logo02=pygame.image.load('teniisss.png')
        self.rec1=self.logo02.get_rect()
        self.rec1.x=300
        self.rec1.y=50
        self.logo01G=pygame.image.load('RunEmoji02.png')
        self.logo02G=pygame.image.load('Logo Ping-Pong.png')
        self.texte=pygame.image.load('Texte.png')
        self.texte1=pygame.image.load('Texte02.png')
        self.texte2=pygame.image.load('Texte03.png')
        self.texte3=pygame.image.load('Texte04.png')

"""
De même, importation des images de fin de jeu, quand le presonnage gagne, ou perd.
"""
class Fin_de_jeu(Aceuill):
    def __init__(self):
        self.mort=pygame.image.load('perdu sad screen.jpg')
        self.fleche=pygame.image.load('Fleche.png')
        self.flecherec=self.fleche.get_rect()
        self.gagner=pygame.image.load('Win.png')

#appel des deux class
ac=Aceuill()
fin=Fin_de_jeu()

"""
class importante, grace à la méthode étape toutes les fenêtres du jeu vont être défini.
Grâce à l'Attribut misazero on peut rejouer plusieurs fois au même jeu
Egalement importation de tous les fonds des jeu
"""
class Jeu(Aceuill):
    def __init__(self):
        self.background=ac.background
        self.etape=1
        self.misazero=0
        self.backgroundpong=pygame.image.load('pong map.jpg')
        self.texte1=pygame.image.load('Playertexte.png')
        self.texte2=pygame.image.load('Opponenttexte.png')


"""
Dans cette class on trouve l'animation et la positions du personnage du jeu de course
"""
class Run(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.vitesse=30
        self.hauteur=1
        self.sprites = []
        self.sprites.append(pygame.image.load('Run/Run1.png'))
        self.sprites.append(pygame.image.load('Run/Run2.png'))
        self.sprites.append(pygame.image.load('Run/Run3.png'))
        self.sprites.append(pygame.image.load('Run/Run4.png'))
        self.current_sprite = 0
        self.image = self.sprites[self.current_sprite]
        self.rect = self.image.get_rect()
        self.rect.x=100
        self.rect.y=75
    """
    animation du personnage
    """
    def update(self,speed):
        self.current_sprite += speed
        if int(self.current_sprite) >= len(self.sprites):
            self.current_sprite = 0
        self.image = self.sprites[int(self.current_sprite)]
    """
    mouvement du personnage
    """
    def monter(self):
        if self.hauteur==3:
            print('Vous ne pouvez monter plus haut')
        else:
            self.hauteur+=1
            self.rect.y-=self.vitesse
    def decendre(self):
        if self.hauteur==1:
            print('Vous ne puvez pas décendre plus bas')
        else:
            self.hauteur-=1
            self.rect.y+=self.vitesse

"""
Dans cette class définition de l'obstacle, ça position
"""
class ObstacleRun(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image=pygame.image.load('OBSTACLES.png')
        self.rect=self.image.get_rect()
        self.vitesse=5
        self.présence=0
        self.rect.x=500
        self.un=200
        self.deux=170
        self.trois=140
        self.rect.y=170
        self.hauteur=2
    def hauteut_de_piste(self):
        a=randint(1,3)
        if self.rect.x==600:
            if a==1:
                self.rect.y=self.un
                self.hauteur=a
            if a==2:
                self.rect.y=self.deux
                self.hauteur=a
            if a==3:
                self.rect.y=self.trois
                self.hauteur=a

"""
Appel des class de cources
"""
moving_sprites = pygame.sprite.Group()
player = Run()
moving_sprites.add(player)
obstacle=ObstacleRun()
jeu=Jeu()

"""
class qui prend les images ainsi que leurs positions pour le jeu pong
"""
class Pong(Aceuill):
    def __init__(self,image,longeur,largeur,a,b):
        self.image=pygame.image.load(image)
        self.rec=pygame.Rect(longeur, largeur , a, b)

"""
Appel des class de pong
"""
playeur=Pong('Raquette 2.png',360,125,10,60)
opponent=Pong('Raquette 1.png',40,125,10,60)
ball=Pong('balle.png',196,125,20,20)
"""
vitesse de la balle de ping-pong
"""
x_speed, y_speed = 2, 2

"""
boucle de jeu
"""
while running:
    screen.blit(jeu.background,(0,0))
    if jeu.etape==1:
        screen.blit(ac.image,(0,0))
        screen.blit(ac.bouton,(175,150))
    if jeu.etape==2:
        screen.blit(ac.logo01,(ac.rec.x,ac.rec.y))
        screen.blit(ac.texte2,(-125,-50))
        screen.blit(ac.logo02,(ac.rec1.x,ac.rec1.y))
        screen.blit(ac.texte3,(125,-50))
    if jeu.etape==3:
        screen.blit(ac.logo01G,(100,50))
        screen.blit(ac.texte,(0,130))
        screen.blit(ac.texte1,(0,30))
        screen.blit(ac.texte2,(10,-50))
        screen.blit(fin.fleche,(10,-5))
    if jeu.etape==4:
        temps.temps()
        if jeu.misazero==1:
            obstacle.vitesse=5
            obstacle.rect.x=500
            obstacle.rect.y=170
            obstacle.hauteur=2
            player.rect.x=100
            player.rect.y=75
            player.hauteur=1
            jeu.misazero=0
        obstacle.rect.x+=obstacle.vitesse/1.5
        if obstacle.hauteur==1 or obstacle.hauteur==2 and player.hauteur==3:
            screen.blit(player.image,(player.rect.x,player.rect.y))
            screen.blit(obstacle.image,(obstacle.rect.x,obstacle.rect.y))
        elif obstacle.hauteur==3 or obstacle.hauteur==2 and player.hauteur==1:
            screen.blit(obstacle.image,(obstacle.rect.x,obstacle.rect.y))
            screen.blit(player.image,(player.rect.x,player.rect.y))
        else:
            screen.blit(obstacle.image,(obstacle.rect.x,obstacle.rect.y))
            screen.blit(player.image,(player.rect.x,player.rect.y))
        obstacle.rect.x-=(obstacle.vitesse)
        if obstacle.rect.x==0:
            obstacle.rect.x=600
            obstacle.hauteut_de_piste()
        if player.hauteur==obstacle.hauteur and 300>obstacle.rect.x>150:
            jeu.etape=7
        if temps.seconds>60.000:
            jeu.etape=8
    if jeu.etape==5:
        screen.blit(ac.logo02G,(150,75))
        screen.blit(ac.texte,(0,130))
        screen.blit(ac.texte1,(0,30))
        screen.blit(ac.texte3,(10,-50))
        screen.blit(fin.fleche,(10,-5))
    if jeu.etape==6:
        if jeu.misazero==1:
            player.rec=pygame.Rect(360,125,10,60)
            opponent.rec=pygame.Rect(40,125,10,60)
            ball.rec=pygame.Rect(196,125,20,20)
            opponent_score=0
            player_score=0
            jeu.misazero=0
        if ball.rec.top >= 300:
            y_speed = random.choice([-1, -2])
        if ball.rec.top <= 0:
            y_speed = random.choice([1, 2])
        if ball.rec.left <= 0:
            player_score += 1
            ball.rec.top=250
            ball.rec.left=150
            x_speed, y_speed = random.choice([2, 1, -1, -2]), random.choice([2, 1, -1, -2])
        if ball.rec.left >= 370:
            opponent_score += 1
            ball.rec.top=250
            ball.rec.left=150
            x_speed, y_speed = random.choice([2, 1, -1, -2]), random.choice([2, 1, -1, -2])
        if playeur.rec.left - ball.rec.width <= ball.rec.left <= playeur.rec.right and ball.rec.top in range(playeur.rec.top-ball.rec.width, playeur.rec.bottom+ball.rec.width):
            x_speed = random.choice([-1, -2])
        if opponent.rec.left - ball.rec.width <= ball.rec.left <= opponent.rec.right and ball.rec.top in range(opponent.rec.top-ball.rec.width, opponent.rec.bottom+ball.rec.width):
            x_speed =  random.choice([1, 2])
        player_score_text = devent.render(str(player_score), True, (250,250,250))
        opponent_score_text = devent.render(str(opponent_score), True, (250,250,250))
        if opponent.rec.top < ball.rec.top:
            opponent.rec.top += 1
        if opponent.rec.bottom > ball.rec.top:
            opponent.rec.bottom -= 1
        ball.rec.left += x_speed * 2
        ball.rec.top += y_speed * 2
        if opponent_score>=20:
            jeu.etape=7
        if player_score>=20:
            jeu.etape=8
        screen.blit(jeu.backgroundpong,(0,0))
        screen.blit(playeur.image,(playeur.rec.left, playeur.rec.top))
        screen.blit(opponent.image,(opponent.rec.left, opponent.rec.top))
        screen.blit(ball.image,(ball.rec.left, ball.rec.top))
        screen.blit(player_score_text,(440,90))
        screen.blit(opponent_score_text,(440,170))
        screen.blit(jeu.texte1,(415,50))
        screen.blit(jeu.texte2,(415,130))
    if jeu.etape==7:
        screen.blit(fin.mort,(0,0))
        screen.blit(fin.fleche,(10,-5))
        jeu.misazero=1
    if jeu.etape==8:
        screen.blit(fin.fleche,(10,-5))
        screen.blit(fin.gagner,(140,30))
        jeu.misazero=1
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==1:
            if ac.recstarte.collidepoint(event.pos):
                jeu.etape=2
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==2:
            if ac.rec.collidepoint(event.pos):
                jeu.etape=3
            elif ac.rec1.collidepoint(event.pos):
                jeu.etape=5
        if event.type==pygame.KEYDOWN and jeu.etape==3:
            temps.TpsZero = pygame.time.get_ticks()
            jeu.etape=4
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==3:
            if fin.flecherec.collidepoint(event.pos):
                jeu.etape=2
        elif event.type==pygame.KEYDOWN and jeu.etape==4:
            if event.key==pygame.K_UP:
                player.monter()
            elif event.key==pygame.K_DOWN:
                player.decendre()
        if event.type==pygame.KEYDOWN and jeu.etape==5:
            jeu.etape=6
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==5:
            if fin.flecherec.collidepoint(event.pos):
                jeu.etape=2
        if event.type==pygame.KEYDOWN and jeu.etape==6:
            if event.key==pygame.K_UP and playeur.rec.top>20:
                playeur.rec.top-=20
            elif event.key==pygame.K_DOWN and playeur.rec.top<210:
                playeur.rec.top+=20
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==7:
            if fin.flecherec.collidepoint(event.pos):
                jeu.etape=2
        if event.type==pygame.MOUSEBUTTONUP and jeu.etape==8:
            if fin.flecherec.collidepoint(event.pos):
                jeu.etape=2

    moving_sprites.update(0.15)
    pygame.display.flip()



    pygame.display.flip()

    clock.tick(60)

pygame.quit()